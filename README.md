# README

Ynsta | "Insta like" projetc for school with Ruby On Rails

## Installation
1. Clone the repository
2. ``` cd ynsta ```
3. ``` bundle install ```
4. ``` rails db:migrate ```
5. ``` rails s ```
6. Go to 'localhost:3000'
7. Create an account, only the first user (with id = 1) can go to the admin dashboard (simpler for the demo to create the tags and the categories)
8. In the admin dashboard create at least one category and one tags
9. Go to home and click on 'Uploader une image'
10. Select a category and some tags and upload an image and then click on 'Publier la photo'

- In the navbar you can:
  1. Click on the logo to go the home page
  2. Show the pictures by category
  3. Search pictures of a user(ex: Pierre), search pictures by tags with an # (ex: #summer)
  4. Go the admin dashboard if you can
  5. Upload an new image
  6. Go to your user page (with your pictures)
  7. Disconnect

- On a picture you can:
  1. Like or dislike if you are connected
  2. Click on the picture's owner name to see his profile
  3. Click on tags to see other pictures of the tag

## Result

![Ynsta Result](https://github.com/jgrandchavin/ynsta/blob/master/public/result.png)
